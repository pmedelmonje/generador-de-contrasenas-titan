import time
import random
import string
import flet as ft



def container_shadow():
    return ft.BoxShadow(
        spread_radius = 3,
        blur_radius = 12,
        color = ft.colors.GREY_600,
        offset = ft.Offset(5,5),
    )


class TitleContainer(ft.UserControl):
    def build(self):

        self.border = 10

        self.title_text = ft.Row(
            alignment = ft.MainAxisAlignment.CENTER,
            controls = [
                ft.Text(
                    "Titán",
                    # tamaño
                    size = 40,
                    weight = "w600",
                    color = "white",
                    text_align = ft.TextAlign.CENTER,
                )
            ]
        )

        self.subtitle_text = ft.Row(
            alignment = ft.MainAxisAlignment.CENTER,
            controls = [
                ft.Text(
                    "Generador de contraseñas levemente paranoico",
                    size = 17,
                    color = ft.colors.WHITE,
                    overflow = ft.TextOverflow.ELLIPSIS,
                ),
            ],
        )

        return ft.Container(
                padding = ft.padding.all(15),
                width = 600,
                border_radius = ft.border_radius.all(10),
                shadow = container_shadow(),                
                gradient = ft.LinearGradient(
                    begin = ft.alignment.top_left,
                    end = ft.alignment.bottom_right,
                    colors = [ft.colors.BLUE_GREY_700, ft.colors.GREY_500]
                ),
                content = (
                    ft.Column(
                        #horizontal_alignment = "center",
                        spacing = 5,
                        controls = [
                            self.title_text,
                            self.subtitle_text,
                        ],
                    )                        
                ),
            )


class ParametersContainer(ft.UserControl):

    def build(self):

        numbers = list(range(0,10))
        string_numbers = [str(i) for i in numbers]
        lowercase_alphabet = list(string.ascii_lowercase)
        uppercase_alphabet = list(string.ascii_uppercase)
        special_characters = ["#", "@", "-", "_", "+", "*",
                            "$", "%", "&"]

        def slider_changed(e):
            self.slider_text.value = int(e.control.value)
            self.slider_text.update()


        def no_marked_checkboxes():
            if (
                self.upper_checkbox.value == False and
                self.numbers_checkbox.value == False and
                self.lower_checkbox.value == False and
                self.special_checkbox.value == False
            ):
                return True


        def generate_paranoid_password(e):

            total_characters = []
            password = ""

            total_characters.append(random.choice(uppercase_alphabet))
            total_characters.append(random.choice(lowercase_alphabet))
            total_characters.append(random.choice(string_numbers))
            total_characters.append(random.choice(special_characters))

            while len(total_characters) < 64:
                random_characters = random.choice([
                    uppercase_alphabet,
                    lowercase_alphabet,
                    string_numbers,
                    special_characters
                ])
                total_characters.append(random.choice(random_characters))

            random.shuffle(total_characters)

            password = "".join(total_characters)

            self.password_field.content.controls[2].value = password
            self.password_field.update()


        def generate_password(e):

            if no_marked_checkboxes():
                self.controls.append(ft.AlertDialog(
                    title = ft.Text(
                        "Alerta",
                        size = 24,
                        color = ft.colors.BLACK,
                        weight = "w600",
                        text_align = "center"
                    ),
                    content = ft.Text(
                        "Debe haber, al menos, una casilla marcada para poder generar la contraseña",
                        size = 18
                    ),
                    open = True
                    )                
                )
                self.update()
            else:
                total_characters = []
                selected_types = []
                password = ""
                if self.upper_checkbox.value == True:
                    total_characters.append(random.choice(uppercase_alphabet))
                    selected_types.append(uppercase_alphabet)
                if self.lower_checkbox.value == True:
                    total_characters.append(random.choice(lowercase_alphabet))
                    selected_types.append(lowercase_alphabet)
                if self.numbers_checkbox.value == True:
                    total_characters.append(random.choice(string_numbers))
                    selected_types.append(string_numbers)
                if self.special_checkbox.value == True:
                    total_characters.append(random.choice(special_characters))
                    selected_types.append(special_characters)

                while len(total_characters) < int(self.slider_text.value):
                    random_characters = random.choice(selected_types)
                    total_characters.append(random.choice(random_characters))

                random.shuffle(total_characters)

                password = "".join(total_characters)

                self.password_field.content.controls[2].value = password
                self.update()


        def set_alert_text_or_none():
            if self.alert_text.value is not None:
                return ft.Text(
                    color = "red",
                    size = 16,
                    weight = "w500",
                )
                

        self.slider_text = ft.Text(
            "4",
            size = 24,
            weight = "w600",
            color = ft.colors.BLUE_GREY
        )        

        self.title_text = ft.Row(
            alignment = ft.MainAxisAlignment.CENTER,
            controls = [
                ft.Text(
                    "Parámetros",
                    # tamaño
                    size = 20,
                    weight = "w600",
                    color = ft.colors.BLUE_GREY,
                    text_align = ft.TextAlign.CENTER,
                )
            ]
        )

        self.characters_slider = ft.Column(
            controls = [
                ft.Text(
                    "Largo de la contraseña",
                    weight = "w600",
                    size = 16,
                ),
                ft.Row(
                    alignment = ft.MainAxisAlignment.CENTER,
                    controls = [
                        self.slider_text,
                        ft.Slider(
                            min = 4,
                            max = 64,
                            divisions = 60,
                            width = 400,
                            active_color = ft.colors.BLUE_GREY,
                            label = "{value}",
                            on_change = slider_changed,
                        ),                        
                    ]
                )
            ]
        )

        self.upper_checkbox = CustomCheckBox("ABC", False)
        self.lower_checkbox = CustomCheckBox("abc", True)
        self.numbers_checkbox = CustomCheckBox("123", True)
        self.special_checkbox = CustomCheckBox("#@%", False)

        self.alert_text = ft.Text(
            color = "red",
            size = 16,
            weight = "w500",
        )

        self.checkboxes_section = ft.Column(
            controls = [
                ft.Text(
                    "Caracteres que puede contener",
                    weight = "w600",
                    size = 16,
                ),
                ft.Row(
                    spacing = 20,
                    alignment = ft.MainAxisAlignment.SPACE_AROUND,
                    controls = [
                    #crear un objeto para los Checkboxes
                        self.upper_checkbox,
                        self.lower_checkbox,
                        self.numbers_checkbox,
                        self.special_checkbox
                    ]
                ),
            ]
        )

        self.password_field = ft.Container(
            content = (
                ft.Column(
                    spacing = 15,
                    controls = [
                        ft.Row(
                            alignment = ft.MainAxisAlignment.CENTER,
                            controls = [
                                ft.Text(
                                    "Generación de contraseña",
                                    # tamaño
                                    size = 20,
                                    weight = "w600",
                                    color = ft.colors.BLUE_GREY,
                                    text_align = ft.TextAlign.CENTER,
                                )
                            ],
                        ),
                        ft.Text(
                            "Pulse el botón 'Generar' para obtener una contraseña con los parámetros definidos previamente, o 'Paranoide' para obtener una contraseña con el largo máximo y todos los caracteres seleccionados."
                        ),
                        ft.TextField(
                            hint_text = "Contraseña generada",
                            border=ft.InputBorder.NONE,
                            filled = True,
                            #read_only = True,
                            #max_lines = 3,
                            text_size = 12                        
                        ),
                        ft.Row(
                            controls = [
                            ft.ElevatedButton(
                                text = "Generar",
                                bgcolor = ft.colors.BLUE_GREY,
                                color = ft.colors.WHITE,
                                tooltip = "Genera una contraseña según los parámetros especificados",
                                #icon = "password",
                                #icon_color = ft.colors.WHITE,
                                on_click = generate_password,
                            ),
                            ft.ElevatedButton(
                                text = "Paranoide",
                                bgcolor = ft.colors.BLUE_GREY_900,
                                color = ft.colors.WHITE,
                                tooltip = "Genera una contraseña paranoica, sin importar los parámetros definidos",
                                on_click = generate_paranoid_password,
                            )
                            ]
                        )
                    ]
                )
            )
        )

        return ft.Container(
            padding = ft.padding.symmetric(
                horizontal = 20,
                vertical = 15
            ),
            bgcolor = ft.colors.WHITE,
            width = 600,
            border_radius = ft.border_radius.all(10),
            shadow = container_shadow(),
            content = (
                ft.Column(
                    controls = [
                    self.title_text,
                    self.characters_slider,
                    self.checkboxes_section,
                    ft.Divider(),
                    self.password_field,
                    ],
                )
            ),
        )


class CustomCheckBox(ft.UserControl):
    def __init__(self, label, value):
        super().__init__()
        self.label = label
        self.value = value


    def build(self):        

        def switch_state(e):
            self.value = not self.value
            self.update()

        return ft.Checkbox(
            label = self.label,
            value = self.value,
            fill_color = {
                ft.MaterialState.SELECTED: ft.colors.BLUE_GREY,
            },
            on_change = switch_state
        )


class App(ft.UserControl):

    def build(self):

        return ft.Container(
            padding = 18,
            content = (
                ft.Column(
                    spacing = 20,
                    controls = [
                        TitleContainer(),
                        ParametersContainer(),
                    ]
                )
            )
        )


def main(page: ft.Page):

    def custom_page_update(e):
        page.update()


    page.title = "Generador de contraseñas Titán"
    page.window_maximizable = False
    #page.window_resizable = False
    page.alignment = "center"
    page.window_width = 640
    page.window_height = 650
    page.window_min_width = 640
    page.window_min_height = 650
    page.window_max_width = 640
    page.window_max_height = 650
    page.update()
    page.horizontal_alignment = ft.MainAxisAlignment.CENTER
    page.vertical_alignment = ft.MainAxisAlignment.CENTER
    page.theme_mode = "light"
    page.padding = 0
    page.margin = 0
    page.scroll = ft.ScrollMode.AUTO
    page.bgcolor = ft.colors.WHITE

    page.on_error = lambda e: print("Page error:", e.data)

    page.update()

    app = App()

    page.add(app)


if __name__ == "__main__":
    ft.app(target = main, assets_dir = "assets")
